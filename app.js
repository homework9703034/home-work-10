function toggleContent(id) {
  const tabs = document.getElementsByClassName("tabs-title");
  for (let i = 1; i <= 5; i++) {
    const content = document.getElementById(i);
    const element = document.getElementById(`a${i}`);
    element.classList.remove("active");
    if (i === id) {
      content.style.display = "block";
    } else {
      content.style.display = "none";
    }
  }

  const activeElement = document.getElementById(`a${id}`);
  activeElement.classList.add("active");
}
